package cc

/**
  * Created by mark on 23/04/2017.
  */

case class ListNode(value:Int,next:ListNode) {
  def hasNext = next != null

  //def hasNext={ if (next==null) false else true}
   def size: Int ={
    if (hasNext) {
      1 + next.size
    } else 1
}

  def filter(f:Int=>Boolean):ListNode={
    if (hasNext){


    }
  }

  def prepend(elem:Int)= ???
  def delete(elem:Int):ListNode= ???

  override def toString:String={
    if(hasNext){
      value+"->"+next.toString
    } else value.toString
  }
}
